#
# Copyright 2019 The Chromium OS Authors. All rights reserved.
#
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

base-config: &base_config
  name: "{{$device-name}}"
  identity: &identity_common
    platform-name: "Hatch"
    smbios-name-match: "{{$fw-name}}"
    sku-id: "{{$sku-id}}"
  firmware:
    no-firmware: True
  test-label: "{{$device-name}}"
  power: &power_common
    charging-ports: |
      CROS_USBPD_CHARGER0 LEFT
      CROS_USBPD_CHARGER1 RIGHT
    low-battery-shutdown-percent: "4.0"
    suspend-to-idle: "1"
    touchpad-wakeup: "1"
    $has-keyboard-backlight: "1"
    has-keyboard-backlight: "{{$has-keyboard-backlight}}"
    $has-ambient-light-sensor: "1"
    has-ambient-light-sensor: "{{$has-ambient-light-sensor}}"
    $internal-backlight-als-steps: |-
      80.0 55.0 -1 400
      100.0 80.0 100 -1
    internal-backlight-als-steps: "{{$internal-backlight-als-steps}}"
    $internal-backlight-no-als-battery-brightness: "63.0"
    internal-backlight-no-als-battery-brightness: "{{$internal-backlight-no-als-battery-brightness}}"
    disable-dark-resume: "0"
    $keyboard-backlight-user-steps: |-
      0.0
      10.0
      20.0
      40.0
      60.0
      100.0
    keyboard-backlight-user-steps: "{{$keyboard-backlight-user-steps}}"
  audio:
    main:
      $cras_root: "/etc/cras"
      $ucm_root: "/usr/share/alsa/ucm"
      # Variables to be overridden by chromeos/devices/... For example,
      # to add a volume curve specific for Kohaku:
      # 1. Add chromeos-base/chromeos-bsp-hatch/files/kohaku/cras-config/card_settings
      # 2. Add chromeos:
      #          devices:
      #            - $device-name: "kohaku"
      #              $cras_card_config: "{{$device-name}}"
      $alsa_card: "sof-sof_rt5682"
      $cras_card_config: "common"
      $cras_dsp_config: "common"
      # Variables passed to adhd/init/cras.sh for starting CRAS
      cras-config-dir: "{{$device-name}}"
      files:
        # ALSA module config
        - source: "common/audio/alsa-module-config/alsa-hatch.conf"
          destination: "/etc/modprobe.d/alsa-hatch.conf"
        # ALSA UCM config
        - source: "common/audio/ucm-config/{{$alsa_card}}/{{$alsa_card}}.conf"
          destination: "{{$ucm_root}}/{{$alsa_card}}/{{$alsa_card}}.conf"
        - source: "common/audio/ucm-config/{{$alsa_card}}/HiFi.conf"
          destination: "{{$ucm_root}}/{{$alsa_card}}/HiFi.conf"
        # CRAS card config (Volume Curve)
        - source: "{{$cras_card_config}}/audio/cras-config/card_settings"
          destination: "{{$cras_root}}/{{cras-config-dir}}/{{$alsa_card}}"
        # EQ/DRC DSP config
        - source: "{{$cras_dsp_config}}/audio/cras-config/dsp.ini"
          destination: "{{$cras_root}}/{{cras-config-dir}}/dsp.ini"

chromeos:
  devices:
    - $device-name: "unprovisioned_hatch"
      skus:
        - $sku-id: 255
          $fw-name: "Hatch"
          config: *base_config
    - $device-name: "hatch"
      $fw-name: "Hatch"
      skus:
        - $sku-id: 1
          config: *base_config

    - $device-name: "unprovisioned_kohaku"
      skus:
        - $sku-id: 255
          $fw-name: "Kohaku"
          config: *base_config
    - $device-name: "kohaku"
      $fw-name: "Kohaku"
      $alsa_card: "sof-cmlda7219max"
      $cras_card_config: "kohaku"
      $internal-backlight-als-steps: |-
        80.0 47.62 -1 400
        100.0 80.0 100 -1
      $has-ambient-light-sensor: "2"
      $keyboard-backlight-user-steps: |-
        0.0
        30.0
        31.0
        33.0
        35.0
        38.0
      skus:
        - $sku-id: 1
          config: *base_config
    - $device-name: "unprovisioned_kindred"
      skus:
        - $sku-id: 255
          $fw-name: "Kindred"
          config: *base_config
    - $device-name: "kled"
      $fw-name: "Kindred"
      $has-ambient-light-sensor: "0"
      skus:
        - $sku-id: 1
          config: *base_config
        - $sku-id: 2
          config: *base_config
        - $sku-id: 3
          config: *base_config
        - $sku-id: 4
          config: *base_config
    - $device-name: "kindred"
      $fw-name: "Kindred"
      $has-ambient-light-sensor: "0"
      $has-keyboard-backlight: "0"
      $cras_dsp_config: "kindred"
      $cras_card_config: "kindred"
      skus:
        - $sku-id: 21
          config: *base_config
        - $sku-id: 22
          config: *base_config
        - $sku-id: 23
          config: *base_config
        - $sku-id: 24
          config: *base_config
    - $device-name: "unprovisioned_helios"
      skus:
        - $sku-id: 255
          $fw-name: "Helios"
          config:
            <<: *base_config
    - $device-name: "helios"
      $fw-name: "Helios"
      $alsa_card: "sof-cml_rt1011_rt5682"
      $cras_card_config: "helios"
      $cras_dsp_config: "helios"
      skus:
        - $sku-id: 1
          config:
            <<: *base_config
            power:
              <<: *power_common
              $has-ambient-light-sensor: "0"
              $internal-backlight-no-als-battery-brightness: "56.25"
    - $device-name: "unprovisioned_akemi"
      skus:
        - $sku-id: 255
          $fw-name: "Akemi"
          config: *base_config
    - $device-name: "akemi"
      $fw-name: "Akemi"
      skus:
        - $sku-id: 1
          config: *base_config
        - $sku-id: 2
          config: *base_config
    - $device-name: "unprovisioned_dratini"
      skus:
        - $sku-id: 255
          $fw-name: "Dratini"
          config: *base_config
    - $device-name: "dratini"
      $fw-name: "Dratini"
      $has-ambient-light-sensor: "0"
      skus:
        - $sku-id: 1
          $has-keyboard-backlight: 0
          config: *base_config
        - $sku-id: 2
          config: *base_config
        - $sku-id: 3
          config: *base_config
        - $sku-id: 4
          $has-keyboard-backlight: 0
          config: *base_config
    - $device-name: "dragonair"
      $fw-name: "Dratini"
      $has-ambient-light-sensor: "0"
      skus:
        - $sku-id: 21
          $has-keyboard-backlight: 0
          config: *base_config
        - $sku-id: 22
          config: *base_config
