// Copyright 2019 The Chromium OS Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

syntax = "proto3";

package testplans;

option go_package = "go.chromium.org/chromiumos/infra/proto/go/testplans";

import "chromiumos/common.proto";
import "google/protobuf/wrappers.proto";

enum HwTestSuiteType {
  HW_TEST_SUITE_TYPE_UNSPECIFIED = 0;
  TAST = 1;
  AUTOTEST = 2;
}

// An individual Chrome OS test; an element of a test suite.
message Test {
  // A test name, e.g. media_recorder_encode_accel_used_h264
  string name = 1;
}

message TestLevelTweak {
  // The Chrome OS builds to which to apply these test level tweaks.
  oneof TweakTarget {
    // A single Chrome OS build target.
    chromiumos.BuildTarget build_target = 1;
    // All Chrome OS build targets.
    google.protobuf.BoolValue all_build_targets = 2;
  }
  // Type of hardware test suite to which this tweak applies.
  HwTestSuiteType hw_test_suite_type = 3;

  // Tests that should be overriden to be critical.
  // Only valid for Tast suites.
  repeated Test promote_to_critical_tests = 4;
  // Tests that should be overriden to only be informational.
  // Only valid for Tast suites.
  repeated Test demote_to_informational_tests = 5;
  // Tests that should be overriden to be disabled.
  // Valid for Tast and Autotest suites.
  repeated Test disable_tests = 6;
}

// Top-level config message for test level tweaks.
message TestLevelTweakCfg {
  repeated TestLevelTweak test_level_tweaks = 1;
}
