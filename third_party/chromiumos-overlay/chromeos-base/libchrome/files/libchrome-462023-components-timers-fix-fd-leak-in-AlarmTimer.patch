From f7c552c89fa9eebe6009365669500372c60be149 Mon Sep 17 00:00:00 2001
From: Eric Caruso <ejcaruso@chromium.org>
Date: Tue, 16 Jul 2019 10:40:14 -0700
Subject: [PATCH] components/timers: fix fd leak in AlarmTimer

This is fixed upstream but will take a while to roll back
into Chrome OS.

BUG=chromium:984593
TEST=enable/disable wifi repeatedly and see that there is no
  growth of open timerfds

Change-Id: Ib55f0c3b2f2c7ffbe8f5f5bd4f09175c415c58a7
---
 components/timers/alarm_timer_chromeos.cc | 13 ++++++-------
 components/timers/alarm_timer_chromeos.h  |  3 ++-
 2 files changed, 8 insertions(+), 8 deletions(-)

diff --git a/components/timers/alarm_timer_chromeos.cc b/components/timers/alarm_timer_chromeos.cc
index e332466..bc645b5 100644
--- a/components/timers/alarm_timer_chromeos.cc
+++ b/components/timers/alarm_timer_chromeos.cc
@@ -84,7 +84,7 @@ void AlarmTimer::Reset() {
   alarm_time.it_value.tv_nsec =
       (delay.InMicroseconds() % base::Time::kMicrosecondsPerSecond) *
       base::Time::kNanosecondsPerMicrosecond;
-  if (timerfd_settime(alarm_fd_, 0, &alarm_time, NULL) < 0)
+  if (timerfd_settime(alarm_fd_.get(), 0, &alarm_time, NULL) < 0)
     PLOG(ERROR) << "Error while setting alarm time.  Timer will not fire";
 
   // The timer is running.
@@ -101,8 +101,9 @@ void AlarmTimer::Reset() {
     base::debug::TaskAnnotator().DidQueueTask("AlarmTimer::Reset",
                                               *pending_task_);
     alarm_fd_watcher_ = base::FileDescriptorWatcher::WatchReadable(
-        alarm_fd_, base::Bind(&AlarmTimer::OnAlarmFdReadableWithoutBlocking,
-                              weak_factory_.GetWeakPtr()));
+        alarm_fd_.get(),
+        base::Bind(&AlarmTimer::OnAlarmFdReadableWithoutBlocking,
+                   weak_factory_.GetWeakPtr()));
   }
 }
 
@@ -112,7 +113,7 @@ void AlarmTimer::OnAlarmFdReadableWithoutBlocking() {
 
   // Read from |alarm_fd_| to ack the event.
   char val[sizeof(uint64_t)];
-  if (!base::ReadFromFD(alarm_fd_, val, sizeof(uint64_t)))
+  if (!base::ReadFromFD(alarm_fd_.get(), val, sizeof(uint64_t)))
     PLOG(DFATAL) << "Unable to read from timer file descriptor.";
 
   OnTimerFired();
@@ -144,9 +145,7 @@ void AlarmTimer::OnTimerFired() {
   }
 }
 
-bool AlarmTimer::CanWakeFromSuspend() const {
-  return alarm_fd_ != -1;
-}
+bool AlarmTimer::CanWakeFromSuspend() const { return alarm_fd_.is_valid(); }
 
 OneShotAlarmTimer::OneShotAlarmTimer() : AlarmTimer(false, false) {
 }
diff --git a/components/timers/alarm_timer_chromeos.h b/components/timers/alarm_timer_chromeos.h
index 8066704..041fc11 100644
--- a/components/timers/alarm_timer_chromeos.h
+++ b/components/timers/alarm_timer_chromeos.h
@@ -8,6 +8,7 @@
 #include <memory>
 
 #include "base/files/file_descriptor_watcher_posix.h"
+#include "base/files/scoped_file.h"
 #include "base/macros.h"
 #include "base/memory/ref_counted.h"
 #include "base/memory/weak_ptr.h"
@@ -56,7 +57,7 @@ class AlarmTimer : public base::Timer {
   void OnTimerFired();
 
   // Timer file descriptor.
-  const int alarm_fd_;
+  base::ScopedFD alarm_fd_;
 
   // Watches |alarm_fd_|.
   std::unique_ptr<base::FileDescriptorWatcher::Controller> alarm_fd_watcher_;
-- 
2.20.1

