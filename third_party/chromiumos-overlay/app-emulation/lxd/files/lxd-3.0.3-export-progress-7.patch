From ed2627e052dd82b045d8b167b2e61d89cfa42414 Mon Sep 17 00:00:00 2001
From: Joel Hockey <joelhockey@chromium.org>
Date: Thu, 31 Jan 2019 20:09:48 -0800
Subject: [PATCH 7/9] lxd: Send progress info for export and import operations

This is part 5 of a series of patches to add better progress
tracking support for export and import.

Send metadata with progress tracking.  Use metadata keys
* progress_stage: unique key for each operation where
  progress is tracked.  Included values are:
  - 'create_container_from_image_unpack'
  - 'create_image_from_container_tar'
  - 'create_image_from_container_compress'
* progress_percent: percent e.g. '0' - '100'
* progress_speed: speed in bytes per second
* <progress_stage>_progress: Formatted text string
  to be displayed in lxc cli.  E.g.
  'Unpack: 34% (45MB/s)'

Signed-off-by: Joel Hockey <joelhockey@chromium.org>
---
 lxd/containers_post.go |  8 ++++++-
 lxd/images.go          | 54 ++++++++++++++++++++++++++++++++++++++----
 shared/util.go         |  9 +++++++
 3 files changed, 65 insertions(+), 6 deletions(-)

diff --git a/lxd/containers_post.go b/lxd/containers_post.go
index 3c8e95fa..e6d8140a 100644
--- a/lxd/containers_post.go
+++ b/lxd/containers_post.go
@@ -17,6 +17,7 @@ import (
 	"github.com/lxc/lxd/lxd/types"
 	"github.com/lxc/lxd/shared"
 	"github.com/lxc/lxd/shared/api"
+	"github.com/lxc/lxd/shared/ioprogress"
 	"github.com/lxc/lxd/shared/logger"
 	"github.com/lxc/lxd/shared/osarch"
 
@@ -121,7 +122,12 @@ func createFromImage(d *Daemon, req *api.ContainersPost) Response {
 			return err
 		}
 
-		_, err = containerCreateFromImage(d, args, info.Fingerprint, nil)
+		metadata := make(map[string]string)
+		_, err = containerCreateFromImage(d, args, info.Fingerprint, &ioprogress.ProgressTracker{
+			Handler: func(percent, speed int64) {
+				shared.SetProgressMetadata(metadata, "create_container_from_image_unpack", "Unpack", percent, speed)
+				op.UpdateMetadata(metadata)
+			}})
 		return err
 	}
 
diff --git a/lxd/images.go b/lxd/images.go
index 88715c6f..68110fb1 100644
--- a/lxd/images.go
+++ b/lxd/images.go
@@ -149,7 +149,7 @@ func compressFile(compress string, infile io.Reader, outfile io.Writer) error {
  * This function takes a container or snapshot from the local image server and
  * exports it as an image.
  */
-func imgPostContInfo(d *Daemon, r *http.Request, req api.ImagesPost, builddir string) (*api.Image, error) {
+func imgPostContInfo(d *Daemon, r *http.Request, req api.ImagesPost, op *operation, builddir string) (*api.Image, error) {
 	info := api.Image{}
 	info.Properties = map[string]string{}
 	name := req.Source.Name
@@ -191,6 +191,33 @@ func imgPostContInfo(d *Daemon, r *http.Request, req api.ImagesPost, builddir st
 	}
 	defer os.Remove(tarfile.Name())
 
+	// Calculate (close estimate of) total size of tarfile
+	totalSize := int64(0)
+	sumSize := func(path string, fi os.FileInfo, err error) error {
+		if err == nil {
+			totalSize += fi.Size()
+		}
+		return nil
+	}
+
+	err = filepath.Walk(c.RootfsPath(), sumSize)
+	if err != nil {
+		return nil, err
+	}
+
+	// Track progress writing tarfile
+	metadata := make(map[string]string)
+	tarfileProgressWriter := &ioprogress.ProgressWriter{
+		WriteCloser: tarfile,
+		Tracker: &ioprogress.ProgressTracker{
+			Handler: func(percent, speed int64) {
+				shared.SetProgressMetadata(metadata, "create_image_from_container_tar", "Image tar", percent, speed)
+				op.UpdateMetadata(metadata)
+			},
+			Length: totalSize,
+		},
+	}
+
 	sha256 := sha256.New()
 	var compressedPath string
 	var compress string
@@ -208,9 +235,9 @@ func imgPostContInfo(d *Daemon, r *http.Request, req api.ImagesPost, builddir st
 
 	// If there is no compression, then calculate sha256 on tarfile
 	if usingCompression {
-		writer = tarfile
+		writer = tarfileProgressWriter
 	} else {
-		writer = io.MultiWriter(tarfile, sha256)
+		writer = io.MultiWriter(tarfileProgressWriter, sha256)
 		compressedPath = tarfile.Name()
 	}
 
@@ -228,6 +255,23 @@ func imgPostContInfo(d *Daemon, r *http.Request, req api.ImagesPost, builddir st
 		}
 		defer tarfile.Close()
 
+		fi, err := tarfile.Stat()
+		if err != nil {
+			return nil, err
+		}
+
+		// Track progress writing gzipped file
+		metadata = make(map[string]string)
+		tarfileProgressReader := &ioprogress.ProgressReader{
+			ReadCloser: tarfile,
+			Tracker: &ioprogress.ProgressTracker{
+				Length: fi.Size(),
+				Handler: func(percent, speed int64) {
+					shared.SetProgressMetadata(metadata, "create_image_from_container_compress", "Image compress", percent, speed)
+					op.UpdateMetadata(metadata)
+				},
+			},
+		}
 		compressedPath = tarfile.Name() + ".compressed"
 
 		compressed, err := os.Create(compressedPath)
@@ -240,7 +284,7 @@ func imgPostContInfo(d *Daemon, r *http.Request, req api.ImagesPost, builddir st
 		// Calculate sha256 as we compress
 		writer := io.MultiWriter(compressed, sha256)
 
-		err = compressFile(compress, tarfile, writer)
+		err = compressFile(compress, tarfileProgressReader, writer)
 		if err != nil {
 			return nil, err
 		}
@@ -709,7 +753,7 @@ func imagesPost(d *Daemon, r *http.Request) Response {
 			} else {
 				/* Processing image creation from container */
 				imagePublishLock.Lock()
-				info, err = imgPostContInfo(d, r, req, builddir)
+				info, err = imgPostContInfo(d, r, req, op, builddir)
 				imagePublishLock.Unlock()
 			}
 		}
diff --git a/shared/util.go b/shared/util.go
index 85706702..ed4bf5bd 100644
--- a/shared/util.go
+++ b/shared/util.go
@@ -1016,6 +1016,15 @@ func DownloadFileSha512(httpClient *http.Client, useragent string, progress func
 	return downloadFileSha(httpClient, useragent, progress, canceler, filename, url, hash, sha512.New(), target)
 }
 
+func SetProgressMetadata(metadata map[string]string, stage, displayPrefix string, percent, speed int64) {
+	// stage, percent, speed sent for API callers.
+	metadata["progress_stage"] = stage
+	metadata["progress_percent"] = strconv.FormatInt(percent, 10)
+	metadata["progress_speed"] = strconv.FormatInt(speed, 10)
+	// <stage>_progress with formatted text sent for lxc cli.
+	metadata[stage+"_progress"] = fmt.Sprintf("%s: %d%% (%s/s)", displayPrefix, percent, GetByteSizeString(speed, 2))
+}
+
 func downloadFileSha(httpClient *http.Client, useragent string, progress func(progress ioprogress.ProgressData), canceler *cancel.Canceler, filename string, url string, hash string, sha hash.Hash, target io.WriteSeeker) (int64, error) {
 	// Always seek to the beginning
 	target.Seek(0, 0)
-- 
2.20.1.791.gb4d0f1c61a-goog

