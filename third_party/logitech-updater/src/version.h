// Copyright 2018 The Chromium OS Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef SRC_VERSION_H_
#define SRC_VERSION_H_


const char kLogiBinaryVersion[] = "1.0.1.112";

#endif /* SRC_VERSION_H_ */
