// SPDX-License-Identifier: (GPL-2.0 OR MIT)
/*
 * Copyright 2019 Bitland Information Technology Co.LTD
 */

/dts-v1/;
#include "mt8183-kukui.dtsi"

/ {
	model = "MediaTek kodama board";
	compatible = "google,kodama", "google,kukui", "mediatek,mt8183";

	ppvarn_lcd: ppvarn-lcd {
		compatible = "regulator-fixed";
		regulator-name = "ppvarn_lcd";
		pinctrl-names = "default";
		pinctrl-0 = <&ppvarn_lcd_en>;

		enable-active-high;

		gpio = <&pio 66 GPIO_ACTIVE_HIGH>;
	};

	ppvarp_lcd: ppvarp-lcd {
		compatible = "regulator-fixed";
		regulator-name = "ppvarp_lcd";
		pinctrl-names = "default";
		pinctrl-0 = <&ppvarp_lcd_en>;

		enable-active-high;

		gpio = <&pio 166 GPIO_ACTIVE_HIGH>;
	};

	pp1800_lcd: pp1800-lcd {
		compatible = "regulator-fixed";
		regulator-name = "pp1800_lcd";
		pinctrl-names = "default";
		pinctrl-0 = <&pp1800_lcd_en>;

		enable-active-high;

		gpio = <&pio 36 GPIO_ACTIVE_HIGH>;
	};
};

&cpu_thermal {
	sustainable-power = <4500>; /* milliwatts */
};

&i2c0 {
	status = "okay";

	touchscreen: touchscreen@10 {
		compatible = "elan,ekth3500";
		reg = <0x10>;
		interrupt-parent = <&pio>;
		interrupts = <155 IRQ_TYPE_LEVEL_LOW>;
		int-gpio = <&pio 155 0>;
		pinctrl-names = "default";
		pinctrl-0 = <&touch_default>;

		reset-gpios = <&pio 156 1>;
	};
};

&panel {
	status = "okay";
	compatible = "boe,tv101wum-nl6";
	reg = <0>;
	enable-gpios = <&pio 45 0>;
	pinctrl-names = "default";
	pinctrl-0 = <&panel_pins_default>;
	avdd-supply = <&ppvarn_lcd>;
	avee-supply = <&ppvarp_lcd>;
	pp1800-supply = <&pp1800_lcd>;
};

&pio {
	/* 192 lines */
	gpio-line-names =
		"SPI_AP_EC_CS_L",
		"SPI_AP_EC_MOSI",
		"SPI_AP_EC_CLK",
		"I2S3_DO",
		"USB_PD_INT_ODL",
		"",
		"",
		"",
		"",
		"IT6505_HPD_L",
		"I2S3_TDM_D3",
		"SOC_I2C6_1V8_SCL",
		"SOC_I2C6_1V8_SDA",
		"DPI_D0",
		"DPI_D1",
		"DPI_D2",
		"DPI_D3",
		"DPI_D4",
		"DPI_D5",
		"DPI_D6",
		"DPI_D7",
		"DPI_D8",
		"DPI_D9",
		"DPI_D10",
		"DPI_D11",
		"DPI_HSYNC",
		"DPI_VSYNC",
		"DPI_DE",
		"DPI_CK",
		"AP_MSDC1_CLK",
		"AP_MSDC1_DAT3",
		"AP_MSDC1_CMD",
		"AP_MSDC1_DAT0",
		"AP_MSDC1_DAT2",
		"AP_MSDC1_DAT1",
		"",
		"",
		"",
		"",
		"",
		"",
		"OTG_EN",
		"DRVBUS",
		"DISP_PWM",
		"DSI_TE",
		"LCM_RST_1V8",
		"AP_CTS_WIFI_RTS",
		"AP_RTS_WIFI_CTS",
		"SOC_I2C5_1V8_SCL",
		"SOC_I2C5_1V8_SDA",
		"SOC_I2C3_1V8_SCL",
		"SOC_I2C3_1V8_SDA",
		"",
		"",
		"",
		"",
		"",
		"",
		"",
		"",
		"",
		"",
		"",
		"",
		"",
		"",
		"",
		"",
		"",
		"",
		"",
		"",
		"",
		"",
		"",
		"",
		"",
		"",
		"",
		"",
		"",
		"SOC_I2C1_1V8_SDA",
		"SOC_I2C0_1V8_SDA",
		"SOC_I2C0_1V8_SCL",
		"SOC_I2C1_1V8_SCL",
		"AP_SPI_H1_MISO",
		"AP_SPI_H1_CS_L",
		"AP_SPI_H1_MOSI",
		"AP_SPI_H1_CLK",
		"I2S5_BCK",
		"I2S5_LRCK",
		"I2S5_DO",
		"BOOTBLOCK_EN_L",
		"MT8183_KPCOL0",
		"SPI_AP_EC_MISO",
		"UART_DBG_TX_AP_RX",
		"UART_AP_TX_DBG_RX",
		"I2S2_MCK",
		"I2S2_BCK",
		"CLK_5M_WCAM",
		"CLK_2M_UCAM",
		"I2S2_LRCK",
		"I2S2_DI",
		"SOC_I2C2_1V8_SCL",
		"SOC_I2C2_1V8_SDA",
		"SOC_I2C4_1V8_SCL",
		"SOC_I2C4_1V8_SDA",
		"",
		"SCL8",
		"SDA8",
		"FCAM_PWDN_L",
		"",
		"",
		"",
		"",
		"",
		"",
		"",
		"",
		"",
		"",
		"",
		"",
		"",
		"",
		"",
		"",
		"",
		"",
		"",
		"",
		"",
		"",
		"",
		"",
		"",
		"I2S_PMIC",
		"I2S_PMIC",
		"I2S_PMIC",
		"I2S_PMIC",
		"I2S_PMIC",
		"I2S_PMIC",
		"I2S_PMIC",
		"I2S_PMIC",
		"",
		"",
		"",
		"",
		"",
		"",
		/*
		 * AP_FLASH_WP_L is crossystem ABI. Rev1 schematics
		 * call it BIOS_FLASH_WP_R_L.
		 */
		"AP_FLASH_WP_L",
		"EC_AP_INT_ODL",
		"IT6505_INT_ODL",
		"H1_INT_OD_L",
		"",
		"",
		"",
		"",
		"",
		"",
		"",
		"AP_SPI_FLASH_MISO",
		"AP_SPI_FLASH_CS_L",
		"AP_SPI_FLASH_MOSI",
		"AP_SPI_FLASH_CLK",
		"DA7219_IRQ",
		"",
		"",
		"",
		"",
		"",
		"",
		"",
		"",
		"",
		"",
		"",
		"",
		"",
		"",
		"",
		"",
		"",
		"",
		"",
		"",
		"",
		"",
		"",
		"",
		"",
		"";

	ppvarp_lcd_en: ppvarp-lcd-en {
		pins1 {
			pinmux = <PINMUX_GPIO66__FUNC_GPIO66>;
			output-low;
		};
	};

	ppvarn_lcd_en: ppvarn-lcd-en {
		pins1 {
			pinmux = <PINMUX_GPIO166__FUNC_GPIO166>;
			output-low;
		};
	};

	pp1800_lcd_en: pp1800-lcd-en {
		pins1 {
			pinmux = <PINMUX_GPIO36__FUNC_GPIO36>;
			output-low;
		};
	};

	touch_default: touchdefault {
		pin_irq {
			pinmux = <PINMUX_GPIO155__FUNC_GPIO155>;
			input-enable;
			bias-pull-up;
		};

		touch_pin_reset: pin_reset {
			pinmux = <PINMUX_GPIO156__FUNC_GPIO156>;
			output-low;
		};
	};
};

&sound {
	mediatek,headset-codec = <&ts3a227e>;
};

&ts3a227e {
	status = "okay";
};
