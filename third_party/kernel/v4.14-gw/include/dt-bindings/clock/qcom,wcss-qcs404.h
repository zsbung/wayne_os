/* SPDX-License-Identifier: GPL-2.0 */
/*
 * Copyright (c) 2018, The Linux Foundation. All rights reserved.
 */

#ifndef _DT_BINDINGS_CLK_WCSS_QCS404_H
#define _DT_BINDINGS_CLK_WCSS_QCS404_H

#define WCSS_AHBFABRIC_CBCR_CLK			0
#define WCSS_AHBS_CBCR_CLK			1
#define WCSS_TCM_CBCR_CLK			2
#define WCSS_AHBM_CBCR_CLK			3
#define WCSS_AXIM_CBCR_CLK			4
#define WCSS_BCR_CBCR_CLK			5
#define WCSS_LCC_CBCR_CLK			6
#define WCSS_QDSP6SS_XO_CBCR_CLK		7
#define WCSS_QDSP6SS_SLEEP_CBCR_CLK		8
#define WCSS_QDSP6SS_GFMMUX_CLK			9

#define Q6SSTOP_QDSP6SS_RESET			0
#define Q6SSTOP_QDSP6SS_CORE_RESET		1
#define Q6SSTOP_QDSP6SS_BUS_RESET		2
#define Q6SSTOP_BCR_RESET			3
#define Q6SSTOP_CORE_ARCH_RESET			4
#endif
