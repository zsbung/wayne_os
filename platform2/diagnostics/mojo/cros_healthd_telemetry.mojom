// Copyright 2019 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Definitions for the telemetry API exposed by the cros_healthd daemon. This
// API is normally consumed by the browser and the telem command-line tool.

// NOTE: This mojom should be kept in sync with the copy in Chromium's repo in
// src/chrome/services/cros_healthd/public/mojom/cros_healthd_telemetry.mojom.

module chromeos.cros_healthd.mojom;

// Enumeration of each of the retrievable telemetry items.
[Extensible]
enum TelemetryItemEnum {
  kMemTotal = 0,  // Amount of physical RAM.
  kMemFree = 1,  // Amount of unused physical RAM .
  kNumRunnableEntities =
      2,  // Number of currently runnable kernel scheduling entities.
  kNumExistingEntities =
      3,  // Number of kernel scheduling entities that currently exist on the
          // system.
  kTotalIdleTime = 4,  // Total idle time for the system.
  kIdleTimePerCPU = 5,  // Idle time per logical CPU.
};

// Enumeration of each of the retrievable telemetry groups.
[Extensible]
enum TelemetryGroupEnum {
  kMemory = 0,
};

// --------------------- Start of telemetry data definitions -------------------
struct MemTotal {
  int32 mem_total_mebibytes@0;
};

struct MemFree {
  int32 mem_free_mebibytes@0;
};

struct NumRunnableEntities {
  int32 num_runnable_entities@0;
};

struct NumExistingEntities {
  int32 num_existing_entities@0;
};

struct TotalIdleTime {
  string total_idle_time_user_hz@0;
};

struct IdleTimePerCPU {
  array<string> idle_time_per_cpu_user_hz@0;
};

// --------------------- End of telemetry data definitions ---------------------

// Collection of all telemetry data which can be retrieved.
union TelemetryItemData {
  MemTotal mem_total@0;
  MemFree mem_free@1;
  NumRunnableEntities num_runnable_entities@2;
  NumExistingEntities num_existing_entities@3;
  TotalIdleTime total_idle_time@4;
  IdleTimePerCPU idle_time_per_cpu@5;
};

// Associates telemetry data with a TelemetryItemEnum.
struct TelemetryItemWithValue {
  TelemetryItemEnum item@0;
  TelemetryItemData data@1;
};
