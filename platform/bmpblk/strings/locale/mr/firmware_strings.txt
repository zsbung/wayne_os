Chrome OS गहाळ आहे किंवा त्यात बिघाड झाला आहे.
कृपया एक रीकव्हरी USB स्टिक किंवा SD कार्ड घाला.
कृपया एक रीकव्हरी USB स्टिक घाला.
कृपया एक रीकव्हरी SD कार्ड किंवा USB स्टिक घाला (टीप: निळा USB पोर्ट रीकव्हरीसाठी कामी येणार नाही).
कृपया डीव्हाइसच्या मागच्या बाजूला असलेल्या 4 पैकी एका पोर्टमध्ये एक रीकव्हरी USB स्टिक घाला.
तुम्ही घातलेल्या डीव्हाइसमध्ये Chrome OS नाही.
OS पडताळणी बंद आहे
पुन्हा चालू करण्यासाठी SPACE दाबा.
तुम्हाला OS पडताळणी चालू ठेवायची असल्याचे निश्चित करण्यासाठी ENTER दाबा.
तुमची सिस्टम रीबूट होईल आणि स्थानिक डेटा साफ केला जाईल.
मागे जाण्यासाठी, ESC दाबा.
OS पडताळणी चालू आहे.
OS पडताळणी बंद करण्यासाठी, ENTER दाबा.
मदतीसाठी https://google.com/chromeos/recovery वर जा
एरर कोड
रीकव्हरी सुरू करण्यासाठी कृपया सर्व बाह्य डीव्हाइस काढा.
मॉडेल 60061e
OS पडताळणी बंद करण्यासाठी RECOVERY बटण दाबा.
हे डीव्हाइस चालवण्यासाठी कनेक्ट केलेल्या उर्जा पुरवठ्यामध्ये पुरेशी उर्जा नाही.
Chrome OS आता बंद होईल.
कृपया योग्य अ‍ॅडाप्टर वापरा आणि पुन्हा प्रयत्न करा.
कृपया कनेक्ट केलेली सर्व डीव्हाइस काढा आणि रीकव्हरी सुरू करा.
पर्यायी बूटलोडर निवडण्यासाठी संख्या की दाबा:
निदाने रन करण्यासाठी POWER बटण दाबा.
OS पडताळणी बंद करण्यासाठी, पॉवर बटण दाबा.
