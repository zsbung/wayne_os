#!/bin/sh

# Copyright 2018 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

# Helper script that restarts a hung network device.

SCRIPT_NAME="$(basename $0)"
POWER_DOWN_TIME_SECONDS=1

set -e

error() {
  logger -t "${SCRIPT_NAME}" -s "Error: $1"
  exit 1
}

if [ $# -ne 1 ] ; then
  error "Missing network interface name"
fi

eval $(udevadm info --query=property --path="/sys/class/net/${1}" |
       egrep '^(DEVPATH|DEVTYPE|ID_BUS|INTERFACE|SUBSYSTEM)')

if [ "${SUBSYSTEM}" != "net" -o -z "${DEVPATH}" ] ; then
  error "Could not retreive info about device ${1}"
fi

logger -t "${SCRIPT_NAME}" "Restarting network interface ${INTERFACE}"

device_path="$(readlink -f "/sys${DEVPATH}/../..")"

if [ -e "/sys${DEVPATH}/device/driver/module" ]; then
  module_name="$(basename "$(readlink -f "/sys${DEVPATH}/device/driver/module")")"
  rmmod "${module_name}" || error "rmmod '${module_name}' failed ($?)"
fi

if [ "${ID_BUS}" = "pci" ] ; then
  device_remove_path="${device_path}/remove"
  bus_rescan_path="$(readlink -f  "${device_path}/../rescan")"

  if [ ! -e "${device_remove_path}" ] ; then
    error "Device remove path ${device_remove_path} does not exist"
  fi

  if [ ! -e "${bus_rescan_path}" ] ; then
    error "Bus rescan path ${bus_rescan_path} does not exist"
  fi

  echo 1 > "${device_remove_path}"
  sleep ${POWER_DOWN_TIME_SECONDS}
  echo 1 > "${bus_rescan_path}"
elif [ "${ID_BUS}" = "usb" ] ; then
  device_authorize_path="$(readlink -f "${device_path}/../authorized")"
  echo 0 > "${device_authorize_path}"
  sleep ${POWER_DOWN_TIME_SECONDS}
  echo 1 > "${device_authorize_path}"
else
  error "Bus type '${ID_BUS}' is unknown"
fi
