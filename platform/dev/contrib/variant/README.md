# Scripts to Create a New Variant of a Baseboard

Creating a new variant of a baseboard, e.g. making the Kohaku variant
of the Hatch baseboard, involves several tasks. We start by basically
cloning the baseboard's configuration and making small edits to put in
the name of the new variant.

These scripts are a work in progress. For any questions, problems, or
suggestions, please contact the author or a member of the Platforms and
Ecosystems team; please do not contact the Infrastructure team.
