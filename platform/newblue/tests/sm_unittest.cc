/*
 * Copyright 2017 The Chromium OS Authors. All rights reserved.
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 */
#include <gtest/gtest.h>

#if defined(__cplusplus)
extern "C" {
#endif

#include "../persist.h"
#include "../sm.h"

#if defined(__cplusplus)
}
#endif

namespace {

class SecurityManagerTestSuite : public testing::Test {
protected:
  void SetUp() override {}
  void TearDown() override {}
};

/* Bluetooth V4.0+ Volume 6, part C, 1.1 */
TEST_F(SecurityManagerTestSuite, CorrectnessOfSecFuncESample) {
  /* value from MSO to LSO */
  uint8_t key[16] = {
      0x4C, 0x68, 0x38, 0x41, 0x39, 0xF5, 0x74, 0xD8,
      0x36, 0xBC, 0xF3, 0x4E, 0x9D, 0xFB, 0x01, 0xBF,
  };
  uint8_t plainText[16] = {
      0x02, 0x13, 0x24, 0x35, 0x46, 0x57, 0x68, 0x79,
      0xac, 0xbd, 0xce, 0xdf, 0xe0, 0xf1, 0x02, 0x13,
  };
  uint8_t encryptedData[16] = {
      0x99, 0xad, 0x1b, 0x52, 0x26, 0xa3, 0x7e, 0x3e,
      0x05, 0x8e, 0x3b, 0x8e, 0x27, 0xc2, 0xc6, 0x66,
  };

  uint8_t out[16] = {0x00};

  smEncryptForTest(out, plainText, key);

  for (uint16_t i = 0; i < sizeof(encryptedData); ++i)
    ASSERT_EQ(out[i], encryptedData[i]);
}

/* Bluetooth V4.0+ Volume 3, Part H, 2.2.3 */
TEST_F(SecurityManagerTestSuite, CorrectnessOfConfValCalcFuncC1Sample) {
  uint8_t k[16] = {
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  };
  uint8_t r[16] = {
      0x57, 0x83, 0xD5, 0x21, 0x56, 0xAD, 0x6F, 0x0E,
      0x63, 0x88, 0x27, 0x4E, 0xC6, 0x70, 0x2E, 0xE0,
  };
  uint64_t pres = 0x05000800000302;
  uint64_t preq = 0x07071000000101;
  // ia is A6:A5:A4:A3:A2:A1, ra is B6:B5:B4:B3:B2:B1
  uint64_t ia = 0xA1A2A3A4A5A6;
  uint64_t ra = 0xB1B2B3B4B5B6;
  bool iat = true;
  bool rat = false;
  uint8_t expected[16] = {
      0x1e, 0x1e, 0x3f, 0xef, 0x87, 0x89, 0x88, 0xea,
      0xd2, 0xa7, 0x4d, 0xc5, 0xbe, 0xf1, 0x3b, 0x86,
  };

  uint8_t out[16] = {0x00};

  smCalcConfValForTest(out, k, r, pres, preq, iat, ia, rat, ra);

  for (uint16_t i = 0; i < sizeof(expected); ++i)
    ASSERT_EQ(out[i], expected[i]);
}

TEST_F(SecurityManagerTestSuite, CorrectnessOfConfValCalcFuncC1Actual) {
  uint8_t k[16] = {
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  };
  uint8_t r[16] = {
      0xB7, 0xA0, 0x6F, 0x93, 0x96, 0x0F, 0x66, 0xB2,
      0x59, 0xB8, 0x46, 0xA9, 0xBF, 0x3A, 0x70, 0xC3,
  };
  uint64_t pres = 0x07071005000302;
  uint64_t preq = 0x0707102D000401;
  // ia is D3:50:98:CB:45:62, ra is 48:FD:81:89:A5:80
  uint64_t ia = 0x6245CB9850D3;
  uint64_t ra = 0x80A58981FD48;
  bool iat = true;
  bool rat = false;
  uint8_t expected[16] = {
      0x69, 0xC2, 0x42, 0xD9, 0x24, 0x62, 0xC4, 0x5A,
      0x9B, 0x94, 0x9C, 0x9E, 0xAC, 0x97, 0xC9, 0x82,
  };

  uint8_t out[16] = {0x00};

  smCalcConfValForTest(out, k, r, pres, preq, iat, ia, rat, ra);

  for (uint16_t i = 0; i < sizeof(expected); ++i)
    ASSERT_EQ(out[i], expected[i]);
}

/* Bluetooth V4.0+ Volume 3, Part H, 2.2.4 */
TEST_F(SecurityManagerTestSuite, CorrectnessOfSTKCalcFuncS1Sample) {
  uint8_t k[16] = {
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  };
  uint8_t r1[16] = {
      0x00, 0x0F, 0x0E, 0x0D, 0x0C, 0x0B, 0x0A, 0x09,
      0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88,
  };
  uint8_t r2[16] = {
      0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08,
      0x99, 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF, 0x00,
  };
  uint8_t expected[16] = {
      0x9a, 0x1f, 0xe1, 0xf0, 0xe8, 0xb0, 0xf4, 0x9b,
      0x5b, 0x42, 0x16, 0xae, 0x79, 0x6d, 0xa0, 0x62,
  };
  uint8_t out[16] = {0x00};

  smCalcKeyForTest(out, k, r1, r2);

  for (uint16_t i = 0; i < sizeof(expected); ++i)
    ASSERT_EQ(out[i], expected[i]);
}

TEST_F(SecurityManagerTestSuite, CorrectnessOfSTKCalcFuncS1Actual) {
  uint8_t k[16] = {
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  };
  uint8_t r1[16] = {
      0xAC, 0xD6, 0x1D, 0x5E, 0x5E, 0x37, 0x1D, 0x59,
      0xA5, 0xE8, 0x42, 0x35, 0x32, 0xF5, 0x06, 0x0B,
  };
  uint8_t r2[16] = {
      0xA8, 0x1F, 0xF3, 0x47, 0x11, 0xC5, 0x31, 0x2C,
      0xBF, 0x42, 0xFD, 0x4E, 0xBA, 0xD6, 0x5B, 0x8E,
  };
  uint8_t expected[16] = {
      0x4C, 0x21, 0x91, 0x84, 0xFA, 0xF3, 0x1D, 0x41,
      0x92, 0x15, 0x9B, 0x48, 0x0E, 0xEA, 0xF6, 0x77,
  };
  uint8_t out[16] = {0x00};

  smCalcKeyForTest(out, k, r1, r2);

  for (uint16_t i = 0; i < sizeof(expected); ++i)
    ASSERT_EQ(out[i], expected[i]);
}

TEST_F(SecurityManagerTestSuite, CorrectnessOfAddressHashActual) {
  int8_t i;
  uint8_t irk[HCI_LE_KEY_LEN] = {0xF5, 0x08, 0xF8, 0x09, 0x86, 0xCA,
                                 0x17, 0xF2, 0xFF, 0x96, 0x18, 0xBD,
                                 0x5A, 0xF8, 0xAE, 0xE3};
  uint8_t addr[BT_MAC_LEN] = {0x2B, 0x01, 0xCF, 0xCA, 0x8A, 0x5D};
  uint32_t rand = 0, hash = 0;

  for (i = BT_MAC_LEN - 1; i > 2; --i)
    rand = rand << 8 | addr[i];
  for (i = 2; i >= 0; --i)
    hash = hash << 8 | addr[i];

  ASSERT_EQ(smAddressHashForTest(rand, irk), hash);
}

TEST_F(SecurityManagerTestSuite, CorrectnessOfLtksBlocking) {
  struct smKey ltks1[] = {{0xF5, 0x08, 0xF8, 0x09, 0x86, 0xCA, 0x17, 0xF2, 0xFF,
                           0x96, 0x18, 0xBD, 0x5A, 0xF8, 0xAE, 0xE3}};
  struct smKey ltks2[] = {{0xF6, 0x08, 0xF8, 0x09, 0x86, 0xCA, 0x17, 0xF2, 0xFF,
                           0x96, 0x18, 0xBD, 0x5A, 0xF8, 0xAE, 0xE3},
                          {0xF7, 0x08, 0xF8, 0x09, 0x86, 0xCA, 0x17, 0xF2, 0xFF,
                           0x96, 0x18, 0xBD, 0x5A, 0xF8, 0xAE, 0xE3}};

  // count > MAX_BLOCKED_LTKS
  ASSERT_FALSE(smSetBlockedLtks(ltks1, 9));

  // count == 0
  ASSERT_TRUE(smSetBlockedLtks(ltks1, 0));
  ASSERT_FALSE(smIsLtkBlockedForTest(&ltks1[0]));
  ASSERT_FALSE(smIsLtkBlockedForTest(&ltks2[0]));
  ASSERT_FALSE(smIsLtkBlockedForTest(&ltks2[0]));

  // count == 1
  ASSERT_TRUE(smSetBlockedLtks(ltks1, 1));
  ASSERT_TRUE(smIsLtkBlockedForTest(&ltks1[0]));
  ASSERT_FALSE(smIsLtkBlockedForTest(&ltks2[0]));
  ASSERT_FALSE(smIsLtkBlockedForTest(&ltks2[1]));

  // count > 1
  ASSERT_TRUE(smSetBlockedLtks(ltks2, 2));
  ASSERT_FALSE(smIsLtkBlockedForTest(&ltks1[0]));
  ASSERT_TRUE(smIsLtkBlockedForTest(&ltks2[0]));
  ASSERT_TRUE(smIsLtkBlockedForTest(&ltks2[1]));
}

TEST_F(SecurityManagerTestSuite, AuthRequirementResponse) {
  constexpr struct {
    const uint8_t input;
    const uint8_t expectedOutput;
  } resultMatrix[] = {
      {SM_AUTH_REQ_BOND_NO, SM_AUTH_REQ_BOND_NO},
      {(SM_AUTH_REQ_BOND_NO | SM_AUTH_REQ_SC_FLAG), SM_AUTH_REQ_BOND_NO},
      {SM_AUTH_REQ_BOND_BOND, SM_AUTH_REQ_BOND_BOND},
      {(SM_AUTH_REQ_BOND_BOND | SM_AUTH_REQ_SC_FLAG), SM_AUTH_REQ_BOND_BOND},
      {SM_AUTH_REQ_MITM_FLAG, SM_AUTH_REQ_MITM_FLAG},
      {(SM_AUTH_REQ_MITM_FLAG | SM_AUTH_REQ_SC_FLAG), SM_AUTH_REQ_MITM_FLAG},
      {(SM_AUTH_REQ_KEYPRESS_FLAG), 0},
  };

  for (const auto &result : resultMatrix) {
    EXPECT_EQ(result.expectedOutput,
              smAuthRequirementFromRemoteAuthRequirementsForTest(result.input));
  }
}

} // namespace
