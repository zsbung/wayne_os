// Copyright (C) 2015 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Common defaults
// ========================================================

// Using Chrome header files directly could cause -Wunused-parameter errors,
// and this is workaround. Please find the document in include_generator.py
// for details.
// Note: gensrcs does not support exclude_srcs, so filegroup rule is
// introduced.
filegroup {
    name: "libchrome-include-sources",
    srcs: [
        "base/**/*.h",
        "build/**/*.h",
        "components/**/*.h",
        "device/**/*.h",
        "testing/**/*.h",
        "third_party/**/*.h",
        "ui/**/*.h",
    ],
    exclude_srcs: [
        "base/android/**/*",
    ],
}

gensrcs {
    name: "libchrome-include",
    cmd: "$(location libchrome_tools/include_generator.py) $(in) $(out)",
    tool_files: ["libchrome_tools/include_generator.py"],
    export_include_dirs: ["."],
    srcs: [":libchrome-include-sources"],
    output_extension: "h",
}

cc_defaults {
    name: "libchrome-defaults",
    // Set clang to "true" to force clang or "false" to force gcc.
    // clang: true,

    cflags: [
        "-Wall",
        "-Werror",
        "-Wno-deprecated-declarations",
        "-Wno-missing-field-initializers",
        "-Wno-unused-parameter",
    ],
    include_dirs: [
        "external/valgrind/include",
        "external/valgrind",
    ],

    // Note: Although the generated header files are exported here, in building
    // libchrome, "." has priority (unlike building projects using libchrome),
    // so the raw header files are used for them.
    generated_headers: ["libchrome-include"],
    export_generated_headers: ["libchrome-include"],
    target: {
        host: {
            cflags: [
                "-D__ANDROID_HOST__",
                "-DDONT_EMBED_BUILD_METADATA",
            ],
        },
        darwin: {
            enabled: false,
        },
    },
}

cc_defaults {
    name: "libchrome-test-defaults",
    defaults: ["libchrome-defaults"],
    cflags: [
        "-Wno-unused-function",
        "-Wno-unused-variable",
    ],

    clang_cflags: [
        // Temporarily suppress the warnings http://b/38232827
        "-Wno-error=unused-lambda-capture",
    ],
}

libchromeCommonSrc = [
    "base/at_exit.cc",
    "base/base64.cc",
    "base/base64url.cc",
    "base/base_paths.cc",
    "base/base_paths_posix.cc",
    "base/base_switches.cc",
    "base/bind_helpers.cc",
    "base/build_time.cc",
    "base/callback_helpers.cc",
    "base/callback_internal.cc",
    "base/command_line.cc",
    "base/cpu.cc",
    "base/debug/activity_tracker.cc",
    "base/debug/alias.cc",
    "base/debug/debugger.cc",
    "base/debug/debugger_posix.cc",
    "base/debug/dump_without_crashing.cc",
    "base/debug/proc_maps_linux.cc",
    "base/debug/profiler.cc",
    "base/debug/stack_trace.cc",
    "base/debug/task_annotator.cc",
    "base/environment.cc",
    "base/feature_list.cc",
    "base/files/file.cc",
    "base/files/file_descriptor_watcher_posix.cc",
    "base/files/file_enumerator.cc",
    "base/files/file_enumerator_posix.cc",
    "base/files/file_path.cc",
    "base/files/file_path_constants.cc",
    "base/files/file_path_watcher.cc",
    "base/files/file_posix.cc",
    "base/files/file_tracing.cc",
    "base/files/file_util.cc",
    "base/files/file_util_posix.cc",
    "base/files/important_file_writer.cc",
    "base/files/memory_mapped_file.cc",
    "base/files/memory_mapped_file_posix.cc",
    "base/files/scoped_file.cc",
    "base/files/scoped_temp_dir.cc",
    "base/guid.cc",
    "base/hash.cc",
    "base/json/json_file_value_serializer.cc",
    "base/json/json_parser.cc",
    "base/json/json_reader.cc",
    "base/json/json_string_value_serializer.cc",
    "base/json/json_value_converter.cc",
    "base/json/json_writer.cc",
    "base/json/string_escape.cc",
    "base/lazy_instance.cc",
    "base/location.cc",
    "base/logging.cc",
    "base/md5.cc",
    "base/memory/aligned_memory.cc",
    "base/memory/ref_counted.cc",
    "base/memory/ref_counted_memory.cc",
    "base/memory/shared_memory_helper.cc",
    "base/memory/singleton.cc",
    "base/memory/weak_ptr.cc",
    "base/message_loop/incoming_task_queue.cc",
    "base/message_loop/message_loop.cc",
    "base/message_loop/message_loop_task_runner.cc",
    "base/message_loop/message_pump.cc",
    "base/message_loop/message_pump_default.cc",
    "base/message_loop/message_pump_libevent.cc",
    "base/metrics/bucket_ranges.cc",
    "base/metrics/field_trial.cc",
    "base/metrics/field_trial_param_associator.cc",
    "base/metrics/metrics_hashes.cc",
    "base/metrics/histogram_base.cc",
    "base/metrics/histogram.cc",
    "base/metrics/histogram_samples.cc",
    "base/metrics/histogram_snapshot_manager.cc",
    "base/metrics/persistent_histogram_allocator.cc",
    "base/metrics/persistent_memory_allocator.cc",
    "base/metrics/persistent_sample_map.cc",
    "base/metrics/sample_map.cc",
    "base/metrics/sample_vector.cc",
    "base/metrics/sparse_histogram.cc",
    "base/metrics/statistics_recorder.cc",
    "base/path_service.cc",
    "base/pending_task.cc",
    "base/pickle.cc",
    "base/posix/global_descriptors.cc",
    "base/posix/file_descriptor_shuffle.cc",
    "base/posix/safe_strerror.cc",
    "base/process/kill.cc",
    "base/process/kill_posix.cc",
    "base/process/launch.cc",
    "base/process/launch_posix.cc",
    "base/process/memory.cc",
    "base/process/process_handle.cc",
    "base/process/process_handle_posix.cc",
    "base/process/process_iterator.cc",
    "base/process/process_metrics.cc",
    "base/process/process_metrics_posix.cc",
    "base/process/process_posix.cc",
    "base/profiler/scoped_profile.cc",
    "base/profiler/scoped_tracker.cc",
    "base/profiler/tracked_time.cc",
    "base/rand_util.cc",
    "base/rand_util_posix.cc",
    "base/run_loop.cc",
    "base/sequence_checker_impl.cc",
    "base/sequence_token.cc",
    "base/sequenced_task_runner.cc",
    "base/sha1.cc",
    "base/strings/pattern.cc",
    "base/strings/safe_sprintf.cc",
    "base/strings/string16.cc",
    "base/strings/string_number_conversions.cc",
    "base/strings/string_piece.cc",
    "base/strings/stringprintf.cc",
    "base/strings/string_split.cc",
    "base/strings/string_util.cc",
    "base/strings/string_util_constants.cc",
    "base/strings/utf_string_conversions.cc",
    "base/strings/utf_string_conversion_utils.cc",
    "base/synchronization/atomic_flag.cc",
    "base/synchronization/condition_variable_posix.cc",
    "base/synchronization/lock.cc",
    "base/synchronization/lock_impl_posix.cc",
    "base/synchronization/read_write_lock_posix.cc",
    "base/synchronization/waitable_event_posix.cc",
    "base/sync_socket_posix.cc",
    "base/sys_info.cc",
    "base/sys_info_posix.cc",
    "base/task/cancelable_task_tracker.cc",
    "base/task_runner.cc",
    "base/task_scheduler/scheduler_lock_impl.cc",
    "base/task_scheduler/scoped_set_task_priority_for_current_thread.cc",
    "base/task_scheduler/sequence.cc",
    "base/task_scheduler/sequence_sort_key.cc",
    "base/task_scheduler/task.cc",
    "base/task_scheduler/task_traits.cc",
    "base/third_party/dynamic_annotations/dynamic_annotations.c",
    "base/third_party/icu/icu_utf.cc",
    "base/third_party/nspr/prtime.cc",
    "base/threading/non_thread_safe_impl.cc",
    "base/threading/platform_thread_posix.cc",
    "base/threading/post_task_and_reply_impl.cc",
    "base/threading/sequenced_task_runner_handle.cc",
    "base/threading/sequenced_worker_pool.cc",
    "base/threading/simple_thread.cc",
    "base/threading/thread.cc",
    "base/threading/thread_checker_impl.cc",
    "base/threading/thread_collision_warner.cc",
    "base/threading/thread_id_name_manager.cc",
    "base/threading/thread_local_storage.cc",
    "base/threading/thread_local_storage_posix.cc",
    "base/threading/thread_restrictions.cc",
    "base/threading/thread_task_runner_handle.cc",
    "base/threading/worker_pool.cc",
    "base/threading/worker_pool_posix.cc",
    "base/time/clock.cc",
    "base/time/default_clock.cc",
    "base/time/default_tick_clock.cc",
    "base/time/tick_clock.cc",
    "base/time/time.cc",
    "base/time/time_posix.cc",
    "base/timer/elapsed_timer.cc",
    "base/timer/timer.cc",
    "base/trace_event/category_registry.cc",
    "base/trace_event/event_name_filter.cc",
    "base/trace_event/heap_profiler_allocation_context.cc",
    "base/trace_event/heap_profiler_allocation_context_tracker.cc",
    "base/trace_event/heap_profiler_allocation_register.cc",
    "base/trace_event/heap_profiler_allocation_register_posix.cc",
    "base/trace_event/heap_profiler_event_filter.cc",
    "base/trace_event/heap_profiler_heap_dump_writer.cc",
    "base/trace_event/heap_profiler_stack_frame_deduplicator.cc",
    "base/trace_event/heap_profiler_type_name_deduplicator.cc",
    "base/trace_event/malloc_dump_provider.cc",
    "base/trace_event/memory_allocator_dump.cc",
    "base/trace_event/memory_allocator_dump_guid.cc",
    "base/trace_event/memory_dump_manager.cc",
    "base/trace_event/memory_dump_provider_info.cc",
    "base/trace_event/memory_dump_request_args.cc",
    "base/trace_event/memory_dump_scheduler.cc",
    "base/trace_event/memory_dump_session_state.cc",
    "base/trace_event/memory_infra_background_whitelist.cc",
    "base/trace_event/memory_usage_estimator.cc",
    "base/trace_event/process_memory_dump.cc",
    "base/trace_event/process_memory_maps.cc",
    "base/trace_event/process_memory_totals.cc",
    "base/trace_event/trace_buffer.cc",
    "base/trace_event/trace_config.cc",
    "base/trace_event/trace_config_category_filter.cc",
    "base/trace_event/trace_event_argument.cc",
    "base/trace_event/trace_event_filter.cc",
    "base/trace_event/trace_event_impl.cc",
    "base/trace_event/trace_event_memory_overhead.cc",
    "base/trace_event/trace_event_synthetic_delay.cc",
    "base/trace_event/trace_log.cc",
    "base/trace_event/trace_log_constants.cc",
    "base/tracked_objects.cc",
    "base/tracking_info.cc",
    "base/unguessable_token.cc",
    "base/values.cc",
    "base/version.cc",
    "base/vlog.cc",
    "device/bluetooth/bluetooth_advertisement.cc",
    "device/bluetooth/bluetooth_uuid.cc",
    "device/bluetooth/bluez/bluetooth_service_attribute_value_bluez.cc",
    "ui/gfx/geometry/insets.cc",
    "ui/gfx/geometry/insets_f.cc",
    "ui/gfx/geometry/point.cc",
    "ui/gfx/geometry/point_conversions.cc",
    "ui/gfx/geometry/point_f.cc",
    "ui/gfx/geometry/rect.cc",
    "ui/gfx/geometry/rect_f.cc",
    "ui/gfx/geometry/size.cc",
    "ui/gfx/geometry/size_conversions.cc",
    "ui/gfx/geometry/size_f.cc",
    "ui/gfx/geometry/vector2d.cc",
    "ui/gfx/geometry/vector2d_f.cc",
    "ui/gfx/range/range.cc",
    "ui/gfx/range/range_f.cc",
]

libchromeLinuxSrc = [
    "base/files/file_path_watcher_linux.cc",
    "base/files/file_util_linux.cc",
    "base/memory/shared_memory_posix.cc",
    "base/memory/shared_memory_tracker.cc",
    "base/posix/unix_domain_socket_linux.cc",
    "base/process/internal_linux.cc",
    "base/process/memory_linux.cc",
    "base/process/process_handle_linux.cc",
    "base/process/process_info_linux.cc",
    "base/process/process_iterator_linux.cc",
    "base/process/process_metrics_linux.cc",
    "base/strings/sys_string_conversions_posix.cc",
    "base/sys_info_linux.cc",
    "base/threading/platform_thread_internal_posix.cc",
    "base/threading/platform_thread_linux.cc",
    "components/timers/alarm_timer_chromeos.cc",
]

libchromeLinuxGlibcSrc = [
    "base/allocator/allocator_shim.cc",
    "base/allocator/allocator_shim_default_dispatch_to_glibc.cc",
    "base/debug/stack_trace_posix.cc",
]

libchromeAndroidSrc = [
    "base/debug/stack_trace_android.cc",
    "base/memory/shared_memory_android.cc",
    "base/sys_info_chromeos.cc",
]

// libchrome static+shared for host and device
// ========================================================
cc_library {
    name: "libchrome",
    host_supported: true,
    vendor_available: true,
    defaults: ["libchrome-defaults"],
    srcs: libchromeCommonSrc,

    export_shared_lib_headers: ["libbase"],
    export_static_lib_headers: ["libgtest_prod"],
    shared_libs: [
        "libbase",
        "libevent",
    ],
    static_libs: [
        "libmodpb64",
        "libgtest_prod",
    ],
    target: {
        linux: {
            srcs: libchromeLinuxSrc,
        },
        linux_glibc: {
            srcs: libchromeLinuxGlibcSrc,
        },
        android: {
            srcs: libchromeAndroidSrc,
            shared_libs: [
                "liblog",
                "libcutils",
            ],
        },
    },
}

// libchrome-crypto shared library for device
// ========================================================

// Similar to libchrome, generate wrapped header files. See comments for
// libchrome-include for the details.
gensrcs {
    name: "libchrome-crypto-include",
    cmd: "$(location libchrome_tools/include_generator.py) $(in) $(out)",
    tool_files: ["libchrome_tools/include_generator.py"],
    export_include_dirs: ["."],
    srcs: ["crypto/**/*.h"],
    output_extension: "h",
}

cc_library_shared {
    name: "libchrome-crypto",
    defaults: ["libchrome-defaults"],
    srcs: [
        "crypto/openssl_util.cc",
        "crypto/random.cc",
        "crypto/secure_hash.cc",
        "crypto/secure_util.cc",
        "crypto/sha2.cc",
    ],

    generated_headers: ["libchrome-crypto-include"],
    export_generated_headers: ["libchrome-crypto-include"],

    shared_libs: [
        "libchrome",
        "libcrypto",
        "libssl",
    ],
}

// Helpers needed for unit tests.
// ========================================================
cc_library_static {
    name: "libchrome_test_helpers",
    defaults: ["libchrome-test-defaults"],
    shared_libs: ["libchrome"],
    host_supported: true,

    srcs: [
        "base/test/gtest_util.cc",
        "base/test/simple_test_clock.cc",
        "base/test/simple_test_tick_clock.cc",
        "base/test/test_file_util.cc",
        "base/test/test_file_util_linux.cc",
        "base/test/test_switches.cc",
        "base/test/test_timeouts.cc",
    ],
}

// Helpers needed for unit tests (for host).
// ========================================================
cc_library_host_static {
    name: "libchrome_test_helpers-host",
    defaults: ["libchrome-test-defaults"],
    shared_libs: ["libchrome"],

    srcs: ["base/test/simple_test_clock.cc"],
}

// Host and target unit tests. Run (from repo root) with:
// ./out/host/<arch>/nativetest/libchrome_test/libchrome_test
// or
// adb shell /data/nativetest/libchrome_test/libchrome_test
// ========================================================
cc_test {
    name: "libchrome_test",
    host_supported: true,
    defaults: ["libchrome-test-defaults"],
    srcs: [
        "base/at_exit_unittest.cc",
        "base/atomicops_unittest.cc",
        "base/base64_unittest.cc",
        "base/base64url_unittest.cc",
        "base/bind_unittest.cc",
        "base/bits_unittest.cc",
        "base/build_time_unittest.cc",
        "base/callback_helpers_unittest.cc",
        "base/callback_list_unittest.cc",
        "base/callback_unittest.cc",
        "base/cancelable_callback_unittest.cc",
        "base/command_line_unittest.cc",
        "base/cpu_unittest.cc",
        "base/debug/activity_tracker_unittest.cc",
        "base/debug/debugger_unittest.cc",
        "base/debug/leak_tracker_unittest.cc",
        "base/debug/task_annotator_unittest.cc",
        "base/environment_unittest.cc",
        "base/files/dir_reader_posix_unittest.cc",
        "base/files/file_descriptor_watcher_posix_unittest.cc",
        "base/files/file_path_watcher_unittest.cc",
        "base/files/file_path_unittest.cc",
        "base/files/file_unittest.cc",
        "base/files/important_file_writer_unittest.cc",
        "base/files/scoped_temp_dir_unittest.cc",
        "base/gmock_unittest.cc",
        "base/guid_unittest.cc",
        "base/id_map_unittest.cc",
        "base/json/json_parser_unittest.cc",
        "base/json/json_reader_unittest.cc",
        "base/json/json_value_converter_unittest.cc",
        "base/json/json_value_serializer_unittest.cc",
        "base/json/json_writer_unittest.cc",
        "base/json/string_escape_unittest.cc",
        "base/lazy_instance_unittest.cc",
        "base/logging_unittest.cc",
        "base/md5_unittest.cc",
        "base/memory/aligned_memory_unittest.cc",
        "base/memory/linked_ptr_unittest.cc",
        "base/memory/ref_counted_memory_unittest.cc",
        "base/memory/ref_counted_unittest.cc",
        "base/memory/scoped_vector_unittest.cc",
        "base/memory/singleton_unittest.cc",
        "base/memory/weak_ptr_unittest.cc",
        "base/message_loop/message_loop_test.cc",
        "base/message_loop/message_loop_task_runner_unittest.cc",
        "base/message_loop/message_loop_unittest.cc",
        "base/metrics/bucket_ranges_unittest.cc",
        "base/metrics/field_trial_unittest.cc",
        "base/metrics/metrics_hashes_unittest.cc",
        "base/metrics/histogram_base_unittest.cc",
        "base/metrics/histogram_macros_unittest.cc",
        "base/metrics/histogram_snapshot_manager_unittest.cc",
        "base/metrics/histogram_unittest.cc",
        "base/metrics/persistent_histogram_allocator_unittest.cc",
        "base/metrics/persistent_memory_allocator_unittest.cc",
        "base/metrics/persistent_sample_map_unittest.cc",
        "base/metrics/sample_map_unittest.cc",
        "base/metrics/sample_vector_unittest.cc",
        "base/metrics/sparse_histogram_unittest.cc",
        "base/metrics/statistics_recorder_unittest.cc",
        "base/numerics/safe_numerics_unittest.cc",
        "base/observer_list_unittest.cc",
        "base/optional_unittest.cc",
        "base/pickle_unittest.cc",
        "base/posix/file_descriptor_shuffle_unittest.cc",
        "base/posix/unix_domain_socket_linux_unittest.cc",
        "base/process/process_info_unittest.cc",
        "base/process/process_metrics_unittest.cc",
        "base/profiler/tracked_time_unittest.cc",
        "base/rand_util_unittest.cc",
        "base/scoped_clear_errno_unittest.cc",
        "base/scoped_generic_unittest.cc",
        "base/security_unittest.cc",
        "base/sequence_checker_unittest.cc",
        "base/sequence_token_unittest.cc",
        "base/sha1_unittest.cc",
        "base/stl_util_unittest.cc",
        "base/strings/pattern_unittest.cc",
        "base/strings/string16_unittest.cc",
        "base/strings/string_number_conversions_unittest.cc",
        "base/strings/string_piece_unittest.cc",
        "base/strings/stringprintf_unittest.cc",
        "base/strings/string_split_unittest.cc",
        "base/strings/string_util_unittest.cc",
        "base/strings/sys_string_conversions_unittest.cc",
        "base/strings/utf_string_conversions_unittest.cc",
        "base/synchronization/atomic_flag_unittest.cc",
        "base/synchronization/condition_variable_unittest.cc",
        "base/synchronization/lock_unittest.cc",
        "base/synchronization/waitable_event_unittest.cc",
        "base/sync_socket_unittest.cc",
        "base/sys_info_unittest.cc",
        "base/task/cancelable_task_tracker_unittest.cc",
        "base/task_runner_util_unittest.cc",
        "base/task_scheduler/scheduler_lock_unittest.cc",
        "base/task_scheduler/scoped_set_task_priority_for_current_thread_unittest.cc",
        "base/task_scheduler/sequence_sort_key_unittest.cc",
        "base/task_scheduler/sequence_unittest.cc",
        "base/task_scheduler/task_traits.cc",
        "base/template_util_unittest.cc",
        "base/test/mock_entropy_provider.cc",
        "base/test/multiprocess_test.cc",
        "base/test/opaque_ref_counted.cc",
        "base/test/scoped_feature_list.cc",
        "base/test/scoped_locale.cc",
        "base/test/sequenced_worker_pool_owner.cc",
        "base/test/test_file_util.cc",
        "base/test/test_file_util_linux.cc",
        "base/test/test_file_util_posix.cc",
        "base/test/test_io_thread.cc",
        "base/test/test_mock_time_task_runner.cc",
        "base/test/test_pending_task.cc",
        "base/test/test_simple_task_runner.cc",
        "base/test/test_switches.cc",
        "base/test/test_timeouts.cc",
        "base/test/trace_event_analyzer.cc",
        "base/threading/non_thread_safe_unittest.cc",
        "base/threading/platform_thread_unittest.cc",
        "base/threading/simple_thread_unittest.cc",
        "base/threading/thread_checker_unittest.cc",
        "base/threading/thread_collision_warner_unittest.cc",
        "base/threading/thread_id_name_manager_unittest.cc",
        "base/threading/thread_local_storage_unittest.cc",
        "base/threading/thread_local_unittest.cc",
        "base/threading/thread_unittest.cc",
        "base/threading/worker_pool_posix_unittest.cc",
        "base/threading/worker_pool_unittest.cc",
        "base/time/pr_time_unittest.cc",
        "base/time/time_unittest.cc",
        "base/timer/hi_res_timer_manager_unittest.cc",
        "base/timer/timer_unittest.cc",
        "base/trace_event/event_name_filter_unittest.cc",
        "base/trace_event/heap_profiler_allocation_context_tracker_unittest.cc",
        "base/trace_event/heap_profiler_stack_frame_deduplicator_unittest.cc",
        "base/trace_event/heap_profiler_type_name_deduplicator_unittest.cc",
        "base/trace_event/memory_allocator_dump_unittest.cc",
        "base/trace_event/memory_dump_manager_unittest.cc",
        "base/trace_event/memory_usage_estimator_unittest.cc",
        "base/trace_event/process_memory_dump_unittest.cc",
        "base/trace_event/trace_config_unittest.cc",
        "base/trace_event/trace_event_argument_unittest.cc",
        "base/trace_event/trace_event_filter_test_utils.cc",
        "base/trace_event/trace_event_synthetic_delay_unittest.cc",
        "base/trace_event/trace_event_unittest.cc",
        "base/tracked_objects_unittest.cc",
        "base/tuple_unittest.cc",
        "base/values_unittest.cc",
        "base/version_unittest.cc",
        "base/vlog_unittest.cc",
        "testing/multiprocess_func_list.cc",
        "testrunner.cc",
        "ui/gfx/range/range_unittest.cc",
    ],

    cflags: ["-DUNIT_TEST"],
    shared_libs: [
        "libchrome",
        "libevent",
    ],
    static_libs: [
        "libgmock",
        "libgtest",
    ],
    target: {
        android: {
            srcs: [
                "crypto/secure_hash_unittest.cc",
                "crypto/sha2_unittest.cc",
            ],
            shared_libs: [
                "libchrome-crypto",
            ],
            cflags: ["-DDONT_EMBED_BUILD_METADATA"],
        },
    },
}

filegroup {
    name: "libmojo_mojom_files",
    srcs: [
        "ipc/ipc.mojom",
        "mojo/common/file.mojom",
        "mojo/common/file_path.mojom",
        "mojo/common/string16.mojom",
        "mojo/common/text_direction.mojom",
        "mojo/common/time.mojom",
        "mojo/common/unguessable_token.mojom",
        "mojo/common/values.mojom",
        "mojo/common/version.mojom",
        "mojo/public/interfaces/bindings/interface_control_messages.mojom",
        "mojo/public/interfaces/bindings/pipe_control_messages.mojom",
        "ui/gfx/geometry/mojo/geometry.mojom",
        "ui/gfx/range/mojo/range.mojom",
    ],
}

filegroup {
    name: "libmojo_mojo_sources",
    srcs: [
        "mojo/**/*.cc",
    ],
    exclude_srcs: [
        // Unused in Chrome. Looks like mistakenly checked in.
        // TODO(hidehiko): Remove this after the file is removed in Chrome
        // repository. http://crrev.com/c/644531
        "mojo/public/cpp/system/message.cc",

        // No WTF support.
        "mojo/public/cpp/bindings/lib/string_traits_wtf.cc",

        // Exclude windows/mac/ios files.
        "**/*_win.cc",
        "mojo/edk/system/mach_port_relay.cc",

        // Exclude js binding related files.
        "mojo/edk/js/**/*",
        "mojo/public/js/**/*",

        // Exclude tests.
        // Note that mojo/edk/embedder/test_embedder.cc needs to be included
        // for Mojo support. cf) b/62071944.
        "**/*_unittest.cc",
        "**/*_unittests.cc",
        "**/*_perftest.cc",
        "mojo/android/javatests/**/*",
        "mojo/edk/system/core_test_base.cc",
        "mojo/edk/system/test_utils.cc",
        "mojo/edk/test/**/*",
        "mojo/public/c/system/tests/**/*",
        "mojo/public/cpp/bindings/tests/**/*",
        "mojo/public/cpp/system/tests/**/*",
        "mojo/public/cpp/test_support/**/*",
        "mojo/public/tests/**/*",
    ],
}

// Python in Chrome repository requires still Python 2.
python_defaults {
    name: "libmojo_scripts",
    version: {
        py2: {
            enabled: true,
        },
        py3: {
            enabled: false,
        },
    },
}

python_binary_host {
    name: "jni_generator",
    main: "base/android/jni_generator/jni_generator.py",
    srcs: [
        "base/android/jni_generator/jni_generator.py",
        "build/**/*.py",
        "third_party/catapult/devil/devil/**/*.py",
    ],
    defaults: ["libmojo_scripts"],
}

python_binary_host {
    name: "mojom_bindings_generator",
    main: "mojo/public/tools/bindings/mojom_bindings_generator.py",
    srcs: [
        "mojo/public/tools/bindings/**/*.py",
        "build/**/*.py",
        "third_party/catapult/devil/**/*.py",
        "third_party/jinja2/**/*.py",
        "third_party/markupsafe/**/*.py",
        "third_party/ply/**/*.py",
    ],
    data: [
        "mojo/public/tools/bindings/generators/cpp_templates/*.tmpl",
        "mojo/public/tools/bindings/generators/java_templates/*.tmpl",
        "mojo/public/tools/bindings/generators/js_templates/*.tmpl",
    ],
    defaults: ["libmojo_scripts"],
}

cc_prebuilt_binary {
    name: "mojom_source_generator_sh",
    srcs: ["libchrome_tools/mojom_source_generator.sh"],
    host_supported: true,
}

genrule {
    name: "libmojo_mojom_headers",
    cmd: "$(location mojom_source_generator_sh)" +
    "    --mojom_bindings_generator=$(location mojom_bindings_generator)" +
    "    --package=external/libchrome" +
    "    --output_dir=$(genDir)" +
    "    --bytecode_path=$(genDir)" +
    "    --typemap=$(location gen/mojo/common/common_custom_types__type_mappings)" +
    "    --generators=c++" +
    "    --use_new_wrapper_types" +
    "    $(in)",

    tools: [
        "mojom_bindings_generator",
        "mojom_source_generator_sh",
    ],

    tool_files: [
        // This file was copied from out/Release in a Chrome checkout.
        // TODO(lhchavez): Generate this file instead of hardcoding it.
        "gen/mojo/common/common_custom_types__type_mappings",
    ],

    srcs: [":libmojo_mojom_files"],

    out: [
        "ipc/ipc.mojom.h",
        "ipc/ipc.mojom-shared.h",
        "ipc/ipc.mojom-shared-internal.h",
        "mojo/common/file.mojom.h",
        "mojo/common/file.mojom-shared.h",
        "mojo/common/file.mojom-shared-internal.h",
        "mojo/common/file_path.mojom.h",
        "mojo/common/file_path.mojom-shared.h",
        "mojo/common/file_path.mojom-shared-internal.h",
        "mojo/common/string16.mojom.h",
        "mojo/common/string16.mojom-shared.h",
        "mojo/common/string16.mojom-shared-internal.h",
        "mojo/common/text_direction.mojom.h",
        "mojo/common/text_direction.mojom-shared.h",
        "mojo/common/text_direction.mojom-shared-internal.h",
        "mojo/common/time.mojom.h",
        "mojo/common/time.mojom-shared.h",
        "mojo/common/time.mojom-shared-internal.h",
        "mojo/common/unguessable_token.mojom.h",
        "mojo/common/unguessable_token.mojom-shared.h",
        "mojo/common/unguessable_token.mojom-shared-internal.h",
        "mojo/common/values.mojom.h",
        "mojo/common/values.mojom-shared.h",
        "mojo/common/values.mojom-shared-internal.h",
        "mojo/common/version.mojom.h",
        "mojo/common/version.mojom-shared.h",
        "mojo/common/version.mojom-shared-internal.h",
        "mojo/public/interfaces/bindings/interface_control_messages.mojom.h",
        "mojo/public/interfaces/bindings/interface_control_messages.mojom-shared.h",
        "mojo/public/interfaces/bindings/interface_control_messages.mojom-shared-internal.h",
        "mojo/public/interfaces/bindings/pipe_control_messages.mojom.h",
        "mojo/public/interfaces/bindings/pipe_control_messages.mojom-shared.h",
        "mojo/public/interfaces/bindings/pipe_control_messages.mojom-shared-internal.h",
        "ui/gfx/geometry/mojo/geometry.mojom.h",
        "ui/gfx/geometry/mojo/geometry.mojom-shared.h",
        "ui/gfx/geometry/mojo/geometry.mojom-shared-internal.h",
        "ui/gfx/range/mojo/range.mojom.h",
        "ui/gfx/range/mojo/range.mojom-shared.h",
        "ui/gfx/range/mojo/range.mojom-shared-internal.h",
    ],
}

genrule {
    name: "libmojo_mojom_srcs",
    cmd: "$(location mojom_source_generator_sh)" +
    "    --mojom_bindings_generator=$(location mojom_bindings_generator)" +
    "    --package=external/libchrome" +
    "    --output_dir=$(genDir)" +
    "    --bytecode_path=$(genDir)" +
    "    --typemap=$(location gen/mojo/common/common_custom_types__type_mappings)" +
    "    --generators=c++" +
    "    --use_new_wrapper_types" +
    "    $(in)",

    tools: [
        "mojom_bindings_generator",
        "mojom_source_generator_sh",
    ],

    tool_files: [
        // This file was copied from out/Release in a Chrome checkout.
        // TODO(lhchavez): Generate this file instead of hardcoding it.
        "gen/mojo/common/common_custom_types__type_mappings",
        "libchrome_tools/mojom_source_generator.sh",
    ],

    srcs: [":libmojo_mojom_files"],

    out: [
        "ipc/ipc.mojom.cc",
        "ipc/ipc.mojom-shared.cc",
        "mojo/common/file.mojom.cc",
        "mojo/common/file.mojom-shared.cc",
        "mojo/common/string16.mojom.cc",
        "mojo/common/string16.mojom-shared.cc",
        "mojo/common/text_direction.mojom.cc",
        "mojo/common/text_direction.mojom-shared.cc",
        "mojo/common/time.mojom.cc",
        "mojo/common/time.mojom-shared.cc",
        "mojo/common/unguessable_token.mojom.cc",
        "mojo/common/unguessable_token.mojom-shared.cc",
        "mojo/common/version.mojom.cc",
        "mojo/common/version.mojom-shared.cc",
        "mojo/public/interfaces/bindings/interface_control_messages.mojom.cc",
        "mojo/public/interfaces/bindings/interface_control_messages.mojom-shared.cc",
        "mojo/public/interfaces/bindings/pipe_control_messages.mojom.cc",
        "mojo/public/interfaces/bindings/pipe_control_messages.mojom-shared.cc",
        "ui/gfx/geometry/mojo/geometry.mojom.cc",
        "ui/gfx/geometry/mojo/geometry.mojom-shared.cc",
        "ui/gfx/range/mojo/range.mojom.cc",
        "ui/gfx/range/mojo/range.mojom-shared.cc",
    ],
}

// TODO(hidehiko): Remove JNI for ContextUtils, after cleaning up the
// depended code.
genrule {
    name: "libmojo_jni_headers",
    cmd: "$(location libchrome_tools/jni_generator_helper.sh)" +
    "    --jni_generator=$(location jni_generator)" +
    "    --output_dir=$(genDir)/jni" +
    "    --includes=base/android/jni_generator/jni_generator_helper.h" +
    "    --ptr_type=long" +
    "    --native_exports_optional" +
    "    $(in)",

    tools: [
        "jni_generator",
    ],

    tool_files: [
        "libchrome_tools/jni_generator_helper.sh",
    ],

    srcs: [
        "base/android/java/src/org/chromium/base/BuildInfo.java",
        "base/android/java/src/org/chromium/base/ContextUtils.java",
        "mojo/android/system/src/org/chromium/mojo/system/impl/BaseRunLoop.java",
        "mojo/android/system/src/org/chromium/mojo/system/impl/CoreImpl.java",
        "mojo/android/system/src/org/chromium/mojo/system/impl/WatcherImpl.java",
    ],

    out: [
        "jni/BuildInfo_jni.h",
        "jni/ContextUtils_jni.h",
        "jni/BaseRunLoop_jni.h",
        "jni/CoreImpl_jni.h",
        "jni/WatcherImpl_jni.h",
    ],
}

cc_library_shared {
    name: "libmojo",

    generated_headers: [
        "libmojo_jni_headers",
        "libmojo_mojom_headers",
    ],

    generated_sources: [
        "libmojo_mojom_srcs",
    ],

    export_generated_headers: [
        "libmojo_jni_headers",
        "libmojo_mojom_headers",
    ],

    srcs: [
        "base/android/build_info.cc",
        "base/android/context_utils.cc",
        "base/android/jni_android.cc",
        "base/android/jni_string.cc",
        "base/android/scoped_java_ref.cc",
        "ipc/ipc_message.cc",
        "ipc/ipc_message_attachment.cc",
        "ipc/ipc_message_attachment_set.cc",
        "ipc/ipc_message_utils.cc",
        "ipc/ipc_mojo_handle_attachment.cc",
        "ipc/ipc_mojo_message_helper.cc",
        "ipc/ipc_mojo_param_traits.cc",
        "ipc/ipc_platform_file_attachment_posix.cc",
        ":libmojo_mojo_sources",
    ],

    cflags: [
        "-Wall",
        "-Werror",
        "-Wno-unused-parameter",
        "-Wno-missing-field-initializers",
        "-DMOJO_EDK_LEGACY_PROTOCOL",
    ],

    // We also pass NO_ASHMEM to make base::SharedMemory avoid using it and prefer
    // the POSIX versions.
    cppflags: [
        "-Wno-sign-promo",
        "-Wno-non-virtual-dtor",
        "-Wno-ignored-qualifiers",
        "-Wno-extra",
        "-DNO_ASHMEM",
    ],

    shared_libs: [
        "libevent",
        "liblog",
        "libchrome",
        "libchrome-crypto",
    ],

    export_include_dirs: ["."],
}

genrule {
    name: "libmojo_mojom_java_srcs",
    cmd: "$(location mojom_source_generator_sh)" +
    "    --mojom_bindings_generator=$(location mojom_bindings_generator)" +
    "    --package=external/libchrome" +
    "    --output_dir=$(genDir)" +
    "    --bytecode_path=$(genDir)" +
    "    --typemap=$(location gen/mojo/common/common_custom_types__type_mappings)" +
    "    --generators=java" +
    "    --use_new_wrapper_types" +
    "    $(in)",

    tools: [
        "mojom_bindings_generator",
        "mojom_source_generator_sh",
    ],

    tool_files: [
        // This file was copied from out/Release in a Chrome checkout.
        // TODO(lhchavez): Generate this file instead of hardcoding it.
        "gen/mojo/common/common_custom_types__type_mappings",
    ],

    srcs: [":libmojo_mojom_files"],

    out: [
        "src/org/chromium/gfx/mojom/InsetsF.java",
        "src/org/chromium/gfx/mojom/Insets.java",
        "src/org/chromium/gfx/mojom/PointF.java",
        "src/org/chromium/gfx/mojom/Point.java",
        "src/org/chromium/gfx/mojom/RangeF.java",
        "src/org/chromium/gfx/mojom/Range.java",
        "src/org/chromium/gfx/mojom/RectF.java",
        "src/org/chromium/gfx/mojom/Rect.java",
        "src/org/chromium/gfx/mojom/SizeF.java",
        "src/org/chromium/gfx/mojom/Size.java",
        "src/org/chromium/gfx/mojom/Vector2dF.java",
        "src/org/chromium/gfx/mojom/Vector2d.java",
        "src/org/chromium/IPC/mojom/ChannelBootstrap_Internal.java",
        "src/org/chromium/IPC/mojom/ChannelBootstrap.java",
        "src/org/chromium/IPC/mojom/Channel_Internal.java",
        "src/org/chromium/IPC/mojom/Channel.java",
        "src/org/chromium/IPC/mojom/GenericInterface_Internal.java",
        "src/org/chromium/IPC/mojom/GenericInterface.java",
        "src/org/chromium/IPC/mojom/IpcConstants.java",
        "src/org/chromium/IPC/mojom/SerializedHandle.java",
        "src/org/chromium/mojo/bindings/interfacecontrol/FlushForTesting.java",
        "src/org/chromium/mojo/bindings/interfacecontrol/InterfaceControlMessagesConstants.java",
        "src/org/chromium/mojo/bindings/interfacecontrol/QueryVersion.java",
        "src/org/chromium/mojo/bindings/interfacecontrol/QueryVersionResult.java",
        "src/org/chromium/mojo/bindings/interfacecontrol/RequireVersion.java",
        "src/org/chromium/mojo/bindings/interfacecontrol/RunInput.java",
        "src/org/chromium/mojo/bindings/interfacecontrol/RunMessageParams.java",
        "src/org/chromium/mojo/bindings/interfacecontrol/RunOrClosePipeInput.java",
        "src/org/chromium/mojo/bindings/interfacecontrol/RunOrClosePipeMessageParams.java",
        "src/org/chromium/mojo/bindings/interfacecontrol/RunOutput.java",
        "src/org/chromium/mojo/bindings/interfacecontrol/RunResponseMessageParams.java",
        "src/org/chromium/mojo/bindings/pipecontrol/DisconnectReason.java",
        "src/org/chromium/mojo/bindings/pipecontrol/PeerAssociatedEndpointClosedEvent.java",
        "src/org/chromium/mojo/bindings/pipecontrol/PipeControlMessagesConstants.java",
        "src/org/chromium/mojo/bindings/pipecontrol/RunOrClosePipeInput.java",
        "src/org/chromium/mojo/bindings/pipecontrol/RunOrClosePipeMessageParams.java",
        "src/org/chromium/mojo/common/mojom/File.java",
        "src/org/chromium/mojo/common/mojom/String16.java",
        "src/org/chromium/mojo/common/mojom/TextDirection.java",
        "src/org/chromium/mojo/common/mojom/TimeDelta.java",
        "src/org/chromium/mojo/common/mojom/Time.java",
        "src/org/chromium/mojo/common/mojom/TimeTicks.java",
        "src/org/chromium/mojo/common/mojom/UnguessableToken.java",
        "src/org/chromium/mojo/common/mojom/Version.java",
    ],
}

java_library {
    name: "android.mojo",

    srcs: [
        ":libmojo_mojom_java_srcs",
        "base/android/java/src/**/*.java",
        "mojo/android/system/src/**/*.java",
        "mojo/public/java/system/src/**/*.java",
        "mojo/public/java/bindings/src/**/*.java",
    ],
}
